﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DTC;
namespace DTC_UnitTest
{
    [TestClass]
    public class Test_isCorrectYearRange
    {
        Form1 testrel = new Form1();
        [TestMethod]
        public void Test_isCorrectYearRange_UTCID01()
        {
            Assert.IsFalse(testrel.isCorrectYearRange(900));
        }
        [TestMethod]
        public void Test_isCorrectYearRange_UTCID02()
        {
            Assert.IsTrue(testrel.isCorrectYearRange(1000));
        }
        [TestMethod]
        public void Test_isCorrectYearRange_UTCID03()
        {
            Assert.IsTrue(testrel.isCorrectYearRange(2000));
        }
        [TestMethod]
        public void Test_isCorrectYearRange_UTCID04()
        {
            Assert.IsTrue(testrel.isCorrectYearRange(3000));
        }
        [TestMethod]
        public void Test_isCorrectYearRange_UTCID05()
        {
            Assert.IsFalse(testrel.isCorrectYearRange(3002));
        }
    }
}
