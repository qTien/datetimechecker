﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DTC;
namespace DTC_UnitTest
{
    [TestClass]
    public class Test_isCorrectMonthRange
    {
        Form1 testrel = new Form1();
        [TestMethod]
        public void Test_isCorrectMonthRange_UTCID01()
        {
            Assert.IsFalse(testrel.isCorrectMonthRange(0));
        }
        [TestMethod]
        public void Test_isCorrectMonthRange_UTCID02()
        {
            Assert.IsTrue(testrel.isCorrectMonthRange(1));
        }
        [TestMethod]
        public void Test_isCorrectMonthRange_UTCID03()
        {
            Assert.IsTrue(testrel.isCorrectMonthRange(5));
        }
        [TestMethod]
        public void Test_isCorrectMonthRange_UTCID04()
        {
            Assert.IsTrue(testrel.isCorrectMonthRange(12));
        }
        [TestMethod]
        public void Test_isCorrectMonthRange_UTCID05()
        {
            Assert.IsFalse(testrel.isCorrectMonthRange(15));
        }
    }
}
