﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DTC;
namespace DTC_UnitTest
{
    [TestClass]
    public class Test_DaysInMonth
    {
        [TestMethod]
        public void Test_DaysInMonth_UTCID01()
        {
            Form1 testrel = new Form1();

            Assert.AreEqual(31,testrel.DayInMonth(3,2001));
        }
        [TestMethod]
        public void Test_DaysInMonth_UTCID02()
        {
            Form1 testrel = new Form1();

            Assert.AreEqual(31, testrel.DayInMonth(5, 2000));
        }
        [TestMethod]
        public void Test_DaysInMonth_UTCID03()
        {
            Form1 testrel = new Form1();

            Assert.AreEqual(30, testrel.DayInMonth(4, 2001));
        }
        [TestMethod]
        public void Test_DaysInMonth_UTCID04()
        {
            Form1 testrel = new Form1();

            Assert.AreEqual(30, testrel.DayInMonth(11, 2000));
        }
        [TestMethod]
        public void Test_DaysInMonth_UTCID05()
        {
            Form1 testrel = new Form1();

            Assert.AreEqual(28, testrel.DayInMonth(2, 2001));
        }
        [TestMethod]
        public void Test_DaysInMonth_UTCID06()
        {
            Form1 testrel = new Form1();

            Assert.AreEqual(29, testrel.DayInMonth(2, 2000));
        }
    }
}
