﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DTC;
namespace DTC_UnitTest
{
    [TestClass]
    public class Test_isCorrectDayRange
    {
        Form1 testrel = new Form1();
        [TestMethod]
        public void Test_isCorrectDayRange_UTCID01()
        {
            Assert.IsFalse(testrel.isCorrectDayRange(0));
        }
        [TestMethod]
        public void Test_isCorrectDayRange_UTCID02()
        {
            Assert.IsTrue(testrel.isCorrectDayRange(1));
        }
        [TestMethod]
        public void Test_isCorrectDayRange_UTCID03()
        {
            Assert.IsTrue(testrel.isCorrectDayRange(15));
        }
        [TestMethod]
        public void Test_isCorrectDayRange_UTCID04()
        {
            Assert.IsTrue(testrel.isCorrectDayRange(31));
        }
        [TestMethod]
        public void Test_isCorrectDayRange_UTCID05()
        {
            Assert.IsFalse(testrel.isCorrectDayRange(35));
        }
    }
}
