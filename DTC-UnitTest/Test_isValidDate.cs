﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DTC;
namespace DTC_UnitTest
{
    [TestClass]
    public class Test_isValidDate
    {
        Form1 testrel = new Form1();
        [TestMethod]
        public void Test_isValidDate_UTCID01()
        {
            Assert.IsTrue(testrel.IsValidDate(28,1,2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID02()
        {

            Assert.IsTrue(testrel.IsValidDate(28, 1, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID03()
        {

            Assert.IsTrue(testrel.IsValidDate(28, 6, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID04()
        {

            Assert.IsTrue(testrel.IsValidDate(28, 6, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID05()
        {

            Assert.IsTrue(testrel.IsValidDate(28, 2, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID06()
        {

            Assert.IsTrue(testrel.IsValidDate(28, 2, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID07()
        {

            Assert.IsTrue(testrel.IsValidDate(29, 1, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID08()
        {

            Assert.IsTrue(testrel.IsValidDate(29, 1, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID09()
        {

            Assert.IsTrue(testrel.IsValidDate(29, 6, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID10()
        {

            Assert.IsTrue(testrel.IsValidDate(29, 6, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID011()
        { 
            Assert.IsFalse(testrel.IsValidDate(29, 2, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID012()
        {
            Assert.IsTrue(testrel.IsValidDate(29, 2, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID013()
        {
            Assert.IsTrue(testrel.IsValidDate(15, 1, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID014()
        {
            Assert.IsTrue(testrel.IsValidDate(15, 1, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID015()
        {
            Assert.IsTrue(testrel.IsValidDate(15, 6, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID016()
        {
            Assert.IsTrue(testrel.IsValidDate(15, 6, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID017()
        {
            Assert.IsTrue(testrel.IsValidDate(15, 2, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID018()
        {
            Assert.IsTrue(testrel.IsValidDate(15, 2, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID019()
        {
            Assert.IsTrue(testrel.IsValidDate(31, 1, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID020()
        {
            Assert.IsTrue(testrel.IsValidDate(31, 1, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID021()
        {
            Assert.IsFalse(testrel.IsValidDate(31, 6, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID022()
        {
            Assert.IsFalse(testrel.IsValidDate(31, 6, 2000));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID023()
        {
            Assert.IsFalse(testrel.IsValidDate(31, 2, 2001));
        }
        [TestMethod]
        public void Test_isValidDate_UTCID024()
        {
            Assert.IsFalse(testrel.IsValidDate(31, 2, 2000));

        }
    }
}

