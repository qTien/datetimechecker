﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DTC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure to exit?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbDay.Clear();
            tbMonth.Clear();
            tbYear.Clear();
        }

        public int DayInMonth(byte month, ushort year)
        {
            switch (month)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    return 31;                    
                case 4:
                case 6:
                case 9:
                case 11:
                    return 30;
                case 2: //tháng 2
                    if (((year % 4 == 0) && !(year % 100 == 0))
                        || (year % 400 == 0))
                        return 29;
                    else
                        return 28;
                default:
                    return 0;
            }
        }
        public bool IsValidDate(byte day, byte month, ushort year)
        {
            if (month >=1 && month<=12)
            {
                if (day >= 1)
                {
                    if(day<=DayInMonth(month,year))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool isCorrectFormat()
        {
            //trường hợp rỗng
            if (tbDay.Text == "" || tbMonth.Text == "" || tbYear.Text == "")
            {
                MessageBox.Show("Please fill all and check again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            } 
            //nếu không phải là số
            if (!Regex.IsMatch(this.tbDay.Text, "^[0-9]*$"))
            {
                MessageBox.Show("Input data for Day is incorrect format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (!Regex.IsMatch(this.tbMonth.Text, "^[0-9]*$"))
            {
                MessageBox.Show("Input data for Month is incorrect format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (!Regex.IsMatch(this.tbYear.Text, "^[0-9]*$"))
            {
                MessageBox.Show("Input data for Year is incorrect format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public bool isCorrectDayRange(byte d)
        {
            //range của các giá trị
            if (1>d || d > 31)
            {
                MessageBox.Show("Input data for Day is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        public bool isCorrectMonthRange(byte m)
        {
            //range của các giá trị
            if (1 > m || m > 12)
            {
                MessageBox.Show("Input data for Month is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        public bool isCorrectYearRange( ushort y)
        {
            if (1000 > y || y > 3000)
            {
                MessageBox.Show("Input data for Year is out of range!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        private void btnCheck_Click(object sender, EventArgs e)
        {
            try
            {
                //Check format
                if (isCorrectFormat())
                {
                    byte d = 0;
                    byte m = 0;
                    ushort y = 0;
                    d = byte.Parse(tbDay.Text);
                    m = byte.Parse(tbMonth.Text);
                    y = ushort.Parse(tbYear.Text);
                    //nếu nằm trong range
                    if(isCorrectDayRange(d))
                    {
                        if(isCorrectMonthRange(m))
                        {
                            if (isCorrectYearRange(y))
                            {  //nếu ngày hợp lệ
                                if (IsValidDate(d, m, y))
                                {
                                    //MessageBox.Show("{0}/{1}/{2} is correct date time!");
                                    MessageBox.Show(d + "/" + m + "/" + y + " is correct date time!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else//ngày không hợp lệ
                                {
                                    //MessageBox.Show("{0}/{1}/{2} is NOT correct date time!");
                                    MessageBox.Show(d + "/" + m + "/" + y + " is NOT correct date time!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                        }
                    }
                }
            }catch(Exception)
            {
                MessageBox.Show("Du lieu nhap qua lon" ,"Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void tbDay_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
